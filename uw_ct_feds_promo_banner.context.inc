<?php
/**
 * @file
 * uw_ct_feds_promo_banner.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_ct_feds_promo_banner_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'feds_promo_banner';
  $context->description = 'Displays promo banner';
  $context->tag = 'Feds';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-feds_promo_banner-block' => array(
          'module' => 'views',
          'delta' => 'feds_promo_banner-block',
          'region' => 'feds_promo_banner',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Displays promo banner');
  t('Feds');
  $export['feds_promo_banner'] = $context;

  return $export;
}
