<?php
/**
 * @file
 * uw_ct_feds_promo_banner.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function uw_ct_feds_promo_banner_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'feds_promo_banner';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Feds promo banner';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '1';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['style_options']['uses_fields'] = TRUE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['inline'] = array(
    'body' => 'body',
    'field_background_image' => 'field_background_image',
  );
  $handler->display->display_options['row_options']['hide_empty'] = TRUE;
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['exclude'] = TRUE;
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['body']['element_default_classes'] = FALSE;
  /* Field: Content: Background Colour */
  $handler->display->display_options['fields']['field_background_colour']['id'] = 'field_background_colour';
  $handler->display->display_options['fields']['field_background_colour']['table'] = 'field_data_field_background_colour';
  $handler->display->display_options['fields']['field_background_colour']['field'] = 'field_background_colour';
  $handler->display->display_options['fields']['field_background_colour']['label'] = '';
  $handler->display->display_options['fields']['field_background_colour']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_background_colour']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_background_colour']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_background_colour']['element_default_classes'] = FALSE;
  /* Field: Content: Background Image */
  $handler->display->display_options['fields']['field_background_image']['id'] = 'field_background_image';
  $handler->display->display_options['fields']['field_background_image']['table'] = 'field_data_field_background_image';
  $handler->display->display_options['fields']['field_background_image']['field'] = 'field_background_image';
  $handler->display->display_options['fields']['field_background_image']['label'] = '';
  $handler->display->display_options['fields']['field_background_image']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_background_image']['alter']['text'] = '<div class="promo-banner">
   <div class="colorpicked">[field_background_colour]</div>
   <div class="promo-bg-image">[field_background_image]</div>   
   <div class="promo-content">[body]</div>
</div>';
  $handler->display->display_options['fields']['field_background_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_background_image']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_background_image']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_background_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_background_image']['settings'] = array(
    'image_style' => 'banner-wide',
    'image_link' => '',
  );
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'feds_promo_banner' => 'feds_promo_banner',
  );
  /* Filter criterion: Content: Promoted to front page */
  $handler->display->display_options['filters']['promote']['id'] = 'promote';
  $handler->display->display_options['filters']['promote']['table'] = 'node';
  $handler->display->display_options['filters']['promote']['field'] = 'promote';
  $handler->display->display_options['filters']['promote']['value'] = '1';

  /* Display: Block-Homepage Promo Banner */
  $handler = $view->new_display('block', 'Block-Homepage Promo Banner', 'block');
  $translatables['feds_promo_banner'] = array(
    t('Master'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('<div class="promo-banner">
   <div class="colorpicked">[field_background_colour]</div>
   <div class="promo-bg-image">[field_background_image]</div>   
   <div class="promo-content">[body]</div>
</div>'),
    t('Block-Homepage Promo Banner'),
  );
  $export['feds_promo_banner'] = $view;

  return $export;
}
